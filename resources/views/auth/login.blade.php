<!DOCTYPE html>
<html lang="en">

@include('includes.header')
    

    <body class="menubar-hoverable header-fixed ">
    
    <!-- BEGIN LOGIN SECTION -->
    <section class="section-account">
        <div class="img-backdrop" style="background-image: url('../../assets/img/modules/materialadmin/img16.jpg')"></div>
        <div class="spacer"></div>
        <div class="card contain-sm style-transparent">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <br/>
                        <span class="text-lg text-bold text-primary">MATERIAL ADMIN</span>
                        <br/><br/>
                        @include('includes.alert')

                        {!! Form::open(array('route' => 'login', 'method' => 'post', 'class' => 'form floating-label')) !!}
                       <!--  <form class="form floating-label" action="http://www.codecovers.eu/materialadmin/dashboards/dashboard" accept-charset="utf-8" method="post"> -->
                            <div class="form-group ">
                                <!-- <div class="col-xs-12"> -->
                                    {!! Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Email Address', 'type'=>'text','autofocus')) !!}
                                <!-- </div> -->
                            </div>

                            <div class="form-group ">

                               <!--  <div class="col-xs-12"> -->
                                    {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password','type'=>'text')) !!}
                                    <p class="help-block"><a data-toggle="modal" href="#myModal">Forgotten?</a></p>
                                <!-- </div> -->
                            </div>
                            <!-- <div class="form-group">
                                <input type="text" class="form-control" id="username" name="username">
                                <label for="username">Username</label>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password" name="password">
                                <label for="password">Password</label>
                                <p class="help-block"><a href="#">Forgotten?</a></p>
                            </div> -->
                            <br/>
                            <div class="row">
                                <div class="col-xs-6 text-left">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            <input type="checkbox"> <span>Remember me</span>
                                        </label>
                                    </div>
                                </div><!--end .col -->

                                <div class="col-xs-6 text-right">
                                {!! Form::submit('Log in', array('class' => 'btn btn-primary btn-raised', 'type'=>'submit')) !!}
                                    <!-- <button class="btn btn-primary btn-raised" type="submit">Login</button> -->
                                </div><!--end .col -->
                            </div><!--end .row -->
                        {!! Form::close() !!}
                    </div><!--end .col -->
                    <div class="col-sm-5 col-sm-offset-1 text-center">
                        <br><br>
                        <h3 class="text-light">
                            No account yet?
                        </h3>
                        <a class="btn btn-block btn-raised btn-primary" href="{{ route('user.create') }}">Sign up here</a> 
                        <br><br>
                        <h3 class="text-light">
                            or
                        </h3>
                        <p>
                            <a href="{{ route('login/fb') }}" class="btn btn-block btn-raised btn-info"><i class="fa fa-facebook pull-left"></i>Login with Facebook</a>
                        </p>
                        <p>
                            <a href="{{ route('login/gp') }}" class="btn btn-block btn-raised btn-info"><i class="fa fa-twitter pull-left"></i>Login with Twitter</a>
                        </p>
                    </div><!--end .col -->
                </div><!--end .row -->
            </div><!--end .card-body -->
        </div><!--end .card -->
    </section>
    <!-- END LOGIN SECTION -->


<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Forgot Password ?</h4>
            </div>
            <div class="modal-body">
                <p>Enter your e-mail address below to reset your password.</p>


                {!! Form::open(array('action' => 'RemindersController@postRemind', 'method' => 'post')) !!}

                {!! Form::email('email', '', array('class' => 'form-control placeholder-no-fix', 'placeholder' => 'Email Address', 'autocomplete'=>'off')) !!}

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>

                {!! Form::submit('Submit', array('class' => 'btn btn-success')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- modal -->


    <!-- BEGIN JAVASCRIPT -->
        {!! HTML::script('../../assets/js/modules/materialadmin/libs/jquery/jquery-1.11.2.min.js') !!}
        {!! HTML::script('../../assets/js/modules/materialadmin/libs/jquery/jquery-migrate-1.2.1.min.js') !!}
        {!! HTML::script('../../assets/js/modules/materialadmin/libs/bootstrap/bootstrap.min.js') !!}
        {!! HTML::script('../../assets/js/modules/materialadmin/libs/spin.js/spin.min.js') !!}
        {!! HTML::script('../../assets/js/modules/materialadmin/libs/autosize/jquery.autosize.min.js') !!}
        {!! HTML::script('../../assets/js/modules/materialadmin/libs/nanoscroller/jquery.nanoscroller.min.js') !!}
        {!! HTML::script('../../assets/js/modules/materialadmin/core/cache/63d0445130d69b2868a8d28c93309746.js') !!}
        {!! HTML::script('../../assets/js/modules/materialadmin/core/demo/Demo.js') !!}

    
    <!-- END JAVASCRIPT -->

    
    </body>
</html>