<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- END META -->
        <link rel="shortcut icon" href="img/favicon_1.ico">


         <!-- END META -->
         <title>{!! $title !!} - {!! Config::get('customConfig.names.siteName')!!}</title>

        <!-- BEGIN STYLESHEETS -->

        <!-- Bootstrap core CSS -->
        {!! Html::style('../../assets/css/modules/materialadmin/css/theme-default/bootstrap94be.css?1422823238') !!}
        {!! Html::style('../../assets/css/modules/materialadmin/css/theme-default/materialadminb0e2.css?1422823243') !!}

                <!--Animation css-->
        {!! Html::style('../../assets/css/modules/materialadmin/css/theme-default/font-awesome.min753e.css?1422823239') !!}


                <!--Icon-fonts css-->
        {!! Html::style('../../assets/css/modules/materialadmin/css/theme-default/material-design-iconic-font.mine7ea.css?1422823240') !!}
        {!! Html::style('../../assets/css/modules/materialadmin/css/theme-default/libs/rickshaw/rickshawd56b.css?1422823372') !!}


                <!--Morris Chart CSS -->
        {!! Html::style('../../assets/css/modules/materialadmin/css/theme-default/libs/morris/morris.core5e0a.css?1422823370') !!}



                <!-- Custom styles for this template -->
        {!! Html::style('http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900') !!}
        <!-- {!! Html::style('css/helper.css') !!} -->

        <!-- END STYLESHEETS -->

                <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        {!! Html::script('js/html5shiv.js') !!}
        {!! Html::script('js/respond.min.js') !!}

        <![endif]-->

        <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

                ga('create', 'UA-62751496-1', 'auto');
                ga('send', 'pageview');

        </script>

</head>