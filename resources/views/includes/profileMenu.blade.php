                <ul class="header-nav header-nav-profile">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                            <img src="../../assets/img/modules/materialadmin/avatar14335.jpg?1422538623" alt="" />
                            <span class="profile-info">
                                Daniel Johnson
                                <small>Administrator</small>
                            </span>
                        </a>
                        <ul class="dropdown-menu animation-dock">
                            <li class="dropdown-header">Config</li>
                            <li><a href="{!!route('profile')!!}">My profile</a></li>
                            <li><a href="../pages/blog/post.html"><span class="badge style-danger pull-right">16</span>My blog</a></li>
                            <li><a href="../pages/calendar.html">My appointments</a></li>
                            <li><a href="{!!route('password.change')!!}">Change Password</a></li>
                            <li class="divider"></li>
                            <li><a href="../pages/locked.html"><i class="fa fa-fw fa-lock"></i> Lock</a></li>
                            <li><a href="{!! route('logout') !!}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                        </ul><!--end .dropdown-menu -->
                    </li><!--end .dropdown -->
                </ul><!--end .header-nav-profile -->