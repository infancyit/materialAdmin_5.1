<!DOCTYPE html>
<html lang="en">

@include('includes.header')
<body class="menubar-hoverable header-fixed ">
@include('includes.topMenu')
@include('includes.sideBar')


	<!-- BEGIN BASE-->
	<div id="base">
		<!-- BEGIN OFFCANVAS LEFT -->
		<div class="offcanvas">
			 		</div><!--end .offcanvas-->
		<!-- END OFFCANVAS LEFT -->

		<!-- BEGIN CONTENT-->
		<div id="content">
			@yield('content')
			
		</div><!--end #content-->		
		<!-- END CONTENT -->



@include('includes.footer')
@yield('script')
</body>
</html>